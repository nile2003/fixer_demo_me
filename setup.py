from setuptools import setup

setup(name='fixer_demo_me',
      version='0.2',
      description='Fixer service demo package',
      url='https://gitlab.com/nile2003/fixer_demo_me',
      author='Hosein',
      author_email='hosein@inprobes.com',
      license='MIT',
      packages=['fixerme'],
      install_requires=['requests'],
      zip_safe=False)
